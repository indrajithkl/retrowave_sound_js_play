(function () {
    var baseUrl = 'http://retrowave.ru';
    var sound;
    var isMobile = true;
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        document.addEventListener("touchend", function(event){
            document.getElementById('playBut').style.display = 'block';
        }, false)
    } else {
        isMobile = false
    }
    function loadSound() {
        var url = new URL("http://retrowave.ru/api/v1/tracks"),
            params = { limit: 5 };
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        var soundList;
        var soundID = "restroWave";
        fetch(url).then(function (resp) { return resp.json() })
            .then(function (response) {
                soundList = response.body.tracks;
                sound = new Howl({
                    src: [`${baseUrl}${soundList[0]['streamUrl']}`],
                    html5: true
                  });
                sound["music_data"] = soundList[0];
                if(!isMobile){
                    debugger;
                    sound.play();
                }
               
            });
    }

    loadSound();

    window.playSound = function(){
        sound.play();
    }
})();